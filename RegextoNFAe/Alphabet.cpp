#include "stdafx.h"
#include "Alphabet.h"

Alphabet::Alphabet() {}

Alphabet::Alphabet(const string& alphabet) {
	_alphabet = alphabet;
}

string Alphabet::getString() const {
	return _alphabet;
}

Alphabet::~Alphabet() {}
