#pragma once

using namespace std;

class Alphabet {
public:
	Alphabet();
	Alphabet(const string& alphabet);
	~Alphabet();
	string getString() const;
private:
	string _alphabet;
};

