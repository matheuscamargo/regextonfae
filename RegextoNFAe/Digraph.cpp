#include "stdafx.h"
#include "Digraph.h"


Digraph::Digraph(int size) {
	_v = size;
	_e = 0;
	_gr = new vector<int>[size + 1];
}


Digraph::~Digraph() {
}

void Digraph::addEdge(int ori, int dest) {
	_e++;
	_gr[ori].push_back(dest);
}

const vector<int>& Digraph::neighbors(int i) const {
	return _gr[i];
}

int Digraph::V() const {
	return _v;
}

int Digraph::E() const {
	return _e;
}
