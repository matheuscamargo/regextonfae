#pragma once
#include "stdafx.h"

using namespace std;

class Digraph {
public:
	Digraph(int size);
	~Digraph();

	void addEdge(int ori, int dest);
	const vector<int> & neighbors(int i) const;
	int V() const;
	int E() const;

private:
	vector<int> *  _gr;
	int _v;
	int _e;
};

