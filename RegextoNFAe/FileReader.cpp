#include "stdafx.h"
#include "FileReader.h"


FileReader::FileReader(string filename) {
	_filename = filename;
}

FileReader::~FileReader() {
}

void FileReader::read() {
	string line;
	ifstream myfile (_filename);

	if (myfile.is_open()) {
		while (getline(myfile, line)) {
			_fileContent.append(line);
			_fileContent.append("\n");
		}

		myfile.close();
	}

	else
		DBOUT("Unable to open file!");
}

string& FileReader::getContent() {
	return _fileContent;
}
