#pragma once

using namespace std;

class FileReader {
public:
	FileReader(string filename);
	~FileReader();
	void read();
	string& getContent();

private:
	string _filename;
	string _fileContent;
};

