#include "stdafx.h"
#include "NFAFileWriter.h"


NFAFileWriter::NFAFileWriter(const NFAe & nfae, string filename) : _nfae(nfae) {
	_filename = filename;
}

NFAFileWriter::~NFAFileWriter() {
}

void NFAFileWriter::write() {
	ofstream myfile(_filename);

	if (myfile.is_open())
	{
		// Print alphabet
		myfile << _nfae.alphabet().getString() << endl;

		// Check non epsilon edges
		string regex = _nfae.regex().getString();
		string nonEpsilon = "";
		int nonEpsilonCount = 0;
		for (int i = 0; i < regex.size(); i++) {
			if (!(('a' <= regex[i] && regex[i] <= 'z') ||
				('A' <= regex[i] && regex[i] <= 'Z'))) continue;
			nonEpsilonCount++;
			nonEpsilon.append(to_string(i + 1));
			nonEpsilon.append(" ");
			nonEpsilon.append(to_string(i + 2));
			nonEpsilon.append(" ");
			nonEpsilon += regex[i];
			nonEpsilon.append("\n");
		}
		// Print number of vertices and edges
		const Digraph & graph = _nfae.states();
		myfile << graph.V() << " " << graph.E() + nonEpsilonCount << endl;

		// Print initial and final state
		myfile << "0" << endl;
		myfile << graph.V() - 1 << endl;

		// Print each epsilon edge
		for (int i = 0; i < graph.V(); i++) {
			for each(auto neigh in graph.neighbors(i)) {
				myfile << i << " " << neigh << endl;
			}
		}

		// Print non epsilon edges
		myfile << nonEpsilon;

		myfile.close();
	}
}
