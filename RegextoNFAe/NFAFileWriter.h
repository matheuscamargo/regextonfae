#pragma once

#include "NFAe.h"

class NFAFileWriter {
public:
	NFAFileWriter(const NFAe & nfae, string filename);
	~NFAFileWriter();
	void write();

private:
	const NFAe & _nfae;
	string _filename;
};

