#include "stdafx.h"
#include "NFAe.h"


NFAe::NFAe(RegularExpression & regex, Alphabet & alphabet, Digraph * states) {
	_regex = regex;
	_alphabet = alphabet;
	_states = states;
}

NFAe::~NFAe() {
}

const RegularExpression & NFAe::regex() const {
	return _regex;
}

const Alphabet & NFAe::alphabet() const {
	return _alphabet;
}

const Digraph & NFAe::states() const {
	return *_states;
}

int NFAe::numberOfeEdges() const {
	return _states->E();
}
