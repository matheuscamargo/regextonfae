#pragma once
#include "Digraph.h"
#include "RegularExpression.h"
#include "Alphabet.h"

class NFAe {
public:
	NFAe(RegularExpression & regex, Alphabet & alphabet, Digraph * states);
	~NFAe();
	const RegularExpression& regex() const;
	const Alphabet& alphabet() const;
	const Digraph& states() const;
	int numberOfeEdges() const;
	

private:
	RegularExpression _regex;
	Digraph * _states;
	Alphabet _alphabet;
};

