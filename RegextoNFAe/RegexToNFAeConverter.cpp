#include "stdafx.h"
#include "RegexToNFAeConverter.h"

RegexToNFAeConverter::RegexToNFAeConverter(RegularExpression & regex, Alphabet& alphabet) {
	_regex = regex;
	_alphabet = alphabet;
	buildNFAe();
}

RegexToNFAeConverter::~RegexToNFAeConverter() {}

const NFAe & RegexToNFAeConverter::getNFAe() const {
	return *_automaton;
}

void RegexToNFAeConverter::buildNFAe() {
	string expression = "(" + _regex.getString() + ")";
	_states = new Digraph(expression.size() + 1);
	int size = expression.size();
	for (int i = 0; i < size; i++) {
		int leftParenthesis = i;

		if (expression[i] == '(' || expression[i] == '|') {
			_stack.push(i);
		}
		else if (expression[i] == ')') {
			int orSymbol = _stack.top();
			_stack.pop();
			if (expression[orSymbol] == '|') {
				leftParenthesis = _stack.top();
				_stack.pop();
				_states->addEdge(leftParenthesis, orSymbol + 1);
				_states->addEdge(orSymbol, i);
			}
			else {
				leftParenthesis = orSymbol;
			}
		}
		if (i < size - 1 && expression[i+1] == '*') {
			_states->addEdge(leftParenthesis, i + 1);
			_states->addEdge(i + 1, leftParenthesis);
		}
		if (expression[i] == '(' ||
				expression[i] == '*' ||
				expression[i] == ')') {
			_states->addEdge(i, i + 1);
		}
	}
	_automaton = new NFAe(_regex, _alphabet, _states);
}