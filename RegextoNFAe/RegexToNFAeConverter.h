#pragma once
#include "stdafx.h"
#include "NFAe.h"
#include "RegularExpression.h"

class RegexToNFAeConverter {

public:
	RegexToNFAeConverter(RegularExpression & regex, Alphabet& alphabet);
	~RegexToNFAeConverter();
	 const NFAe & getNFAe() const;

private:
	RegularExpression  _regex;
	NFAe * _automaton;
	Digraph * _states;
	Alphabet _alphabet;
	std::stack<int> _stack;
	void buildNFAe();
};