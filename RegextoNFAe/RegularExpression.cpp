#include "stdafx.h"
#include "RegularExpression.h"

RegularExpression::RegularExpression() {

}

RegularExpression::RegularExpression(const string& regex) {
	_regex = regex;
}

RegularExpression::~RegularExpression() {
}

string RegularExpression::getString() const {
	return _regex;
}
