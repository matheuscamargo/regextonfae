#pragma once

using namespace std;

class RegularExpression {
public:
	RegularExpression();
	RegularExpression(const string& regex);
	~RegularExpression();
	string getString() const;

private:
	string _regex;
};

