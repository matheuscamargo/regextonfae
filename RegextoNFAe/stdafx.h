// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers



// TODO: reference additional headers your program requires here
#include <fstream>
#include <iostream>
#include <sstream>
#include <Windows.h>

#include <vector>
#include <stack>
#include <string>

//MACROS
#define DBOUT( s )            \
{                             \
   std::wostringstream os_;    \
   os_ << s;                   \
   OutputDebugStringW( os_.str().c_str() );  \
}

//http://stackoverflow.com/questions/1149620/how-to-write-to-the-output-window-in-visual-studio