#include "stdafx.h"
#include "CppUnitTest.h"
#include "../RegextoNFAe/RegularExpression.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace RegextoNFAeTest {

	TEST_CLASS(RegularExpressionTest) {
	public:
		
		TEST_METHOD(TestRegexString) {
			RegularExpression *r = new RegularExpression("test");
			
			std::string t = "test";

			Assert::AreEqual(t, r->getString());
		}
	};
}