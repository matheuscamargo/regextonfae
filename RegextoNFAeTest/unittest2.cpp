#include "stdafx.h"
#include "CppUnitTest.h"
#include "../RegextoNFAe/Filereader.h"

#include <direct.h>
#define GetCurrentDir _getcwd

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace RegextoNFAeTest {

	TEST_CLASS(TestFileReader) {
	public:

		TEST_METHOD(TestReadFile) {
			FileReader * filereader = new FileReader("../../RegextoNFAeTest/regex.txt");
			filereader->read();

			string expectedString = "a b c d\n((a*b|ac)d)\n";
			Assert::AreEqual(expectedString, filereader->getContent());
		}
	};
}