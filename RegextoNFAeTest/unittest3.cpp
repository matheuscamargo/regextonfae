#include "stdafx.h"
#include "CppUnitTest.h"
#include "../RegextoNFAe/RegexToNFAeConverter.h"
#include "../RegextoNFAe/RegularExpression.h"
#include "../RegextoNFAe/Digraph.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace RegextoNFAeTest {

	TEST_CLASS(RegexToNFAeConverterTest) {
	public:

		TEST_METHOD(TestConverter) {
			RegularExpression *regex = new RegularExpression("((a*b|ac)d)");
			Alphabet *alphabet = new Alphabet("a");

			RegexToNFAeConverter * converter = new RegexToNFAeConverter(*regex, *alphabet);
			const NFAe& nfae = converter->getNFAe();

			int expectedNumberOfStates = 11;

			//check states
			Assert::AreEqual(expectedNumberOfStates, nfae.numberOfeEdges());
		}
	};
}