#include "stdafx.h"
#include "CppUnitTest.h"
#include "../RegextoNFAe/NFAFileWriter.h"
#include "../RegextoNFAe/RegexToNFAeConverter.h"
#include "../RegextoNFAe/RegularExpression.h"
#include "../RegextoNFAe/Digraph.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace RegextoNFAeTest {

	TEST_CLASS(NFAeFileWriterTest) {
	public:
		TEST_CLASS_INITIALIZE(setup) {
			DBOUT("IM hereeeeeeee!");
		}

		TEST_METHOD(TestWriteToFile1) {
			regex = new RegularExpression("aaaa");
			alphabet = new Alphabet("a");
			converter = new RegexToNFAeConverter(*regex, *alphabet);
			const NFAe& nfae = converter->getNFAe();

			NFAFileWriter * writer = new NFAFileWriter(nfae, "../../RegextoNFAeTest/regex1.txt");
			writer->write();

			//check if files are the same
			string expectedString = "a\n7 6\n0\n6\n0 1\n5 6\n1 2 a\n2 3 a\n3 4 a\n4 5 a\n";
			string actualString = readFileContent("../../RegextoNFAeTest/regex1.txt");

			Assert::AreEqual(expectedString, actualString);
		}

		TEST_METHOD(TestWriteToFile2) {
			regex = new RegularExpression("a*");
			alphabet = new Alphabet("a");
			converter = new RegexToNFAeConverter(*regex, *alphabet);
			const NFAe& nfae = converter->getNFAe();

			NFAFileWriter * writer = new NFAFileWriter(nfae, "../../RegextoNFAeTest/regex2.txt");
			writer->write();

			//check if files are the same
			string expectedString = "a\n5 6\n0\n4\n0 1\n1 2\n2 1\n2 3\n3 4\n1 2 a\n";
			string actualString = readFileContent("../../RegextoNFAeTest/regex2.txt");

			Assert::AreEqual(expectedString, actualString);
		}

		TEST_METHOD(TestWriteToFile3) {
			regex = new RegularExpression("(a*b|ac)d");
			alphabet = new Alphabet("a b c d");
			converter = new RegexToNFAeConverter(*regex, *alphabet);
			const NFAe& nfae = converter->getNFAe();

			NFAFileWriter * writer = new NFAFileWriter(nfae, "../../RegextoNFAeTest/regex3.txt");
			writer->write();

			//check if files are the same
			string expectedString = "a b c d\n12 14\n0\n11\n0 1\n1 2\n1 6\n2 3\n3 2\n3 4\n5 8\n8 9\n10 11\n2 3 a\n4 5 b\n6 7 a\n7 8 c\n9 10 d\n";
			
			string actualString = readFileContent("../../RegextoNFAeTest/regex3.txt");
			Assert::AreEqual(expectedString, actualString);
		}

		TEST_METHOD(TestWriteToFile4) {
			regex = new RegularExpression("(a|b)*b(a|b)");
			alphabet = new Alphabet("a b c d e");
			converter = new RegexToNFAeConverter(*regex, *alphabet);
			const NFAe& nfae = converter->getNFAe();

			NFAFileWriter * writer = new NFAFileWriter(nfae, "../../RegextoNFAeTest/regex4.txt");
			writer->write();

			//check if files are the same
			string expectedString = "a b c d e\n15 18\n0\n14\n0 1\n1 2\n1 4\n1 6\n3 5\n5 6\n6 1\n6 7\n8 9\n8 11\n10 12\n12 13\n13 14\n2 3 a\n4 5 b\n7 8 b\n9 10 a\n11 12 b\n";

			string actualString = readFileContent("../../RegextoNFAeTest/regex4.txt");
			Assert::AreEqual(expectedString, actualString);
		}

private:
	RegularExpression *regex;
	Alphabet *alphabet;
	RegexToNFAeConverter *converter;

	string readFileContent(string filename) {
		string returnString;

		ifstream myfile(filename);
		string line;

		if (myfile.is_open())
		{
			while (getline(myfile, line))
			{
				returnString += line + "\n";
			}
			myfile.close();
		}

		return returnString;
	}

	};
}